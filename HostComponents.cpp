#include "HostComponents.hpp"

cl_uint Platforms::getPlatformNumber() {
	return platforms.platformCount;
}

cl_platform_id Platforms::getPlatformList() {
	return platforms.platformList;
}

void Platforms::getPlatformIDs() {
	unsigned max_num_entries = 2;
	clGetPlatformIDs(max_num_entries, &platforms.platformList, &platforms.platformCount);
	printf("number of ocl platforms %u\n", platforms.platformCount);
}

Devices::Devices() {
	devices = new cl_device_id[5];
	for (short i = 0; i < 5; ++i){
		gpuDeviceIndex[i] = false;
		cpuDeviceIndex[i] = false;
	}
}

void Devices::getAllPlatformDevices(Platforms platform) {
	cl_device_type devType;

	size_t size;
	for (cl_uint i = 0; i < platform.getPlatformNumber(); ++i){
		clGetDeviceIDs(platform.getPlatformList(), CL_DEVICE_TYPE_ALL, 2, devices, &retNumDevice); // CL_GET_DEVICE_TYPE_GPU
		printf("Number of the devices: %u\n", retNumDevice);
		for (cl_uint s = 0; s < retNumDevice; ++s){
			clGetDeviceInfo(devices[s], CL_DEVICE_TYPE, sizeof(cl_device_type), &devType, &size);
			//printf("devType : %u\n", devType);
			
			if (devType == 4) { // 4 - GPU
				gpuDeviceIndex[s] = true;
			}
			else if (devType == 2) { // 2 - CPU
				cpuDeviceIndex[s] = true;
			}
			else {
				printf("uncompatable device type\n");
			}

			clGetDeviceInfo(devices[s], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(size_t), &numUnits[s], &size);
		}
	}
}

void Devices::initBestDevice() {
	short maxCalcUnits = -1;
	char* devName = new char[100];
	size_t size;
	for (short i = 0; i < 5; ++i){
		if (gpuDeviceIndex[i] && maxCalcUnits < numUnits[i]){
			maxCalcUnits = numUnits[i];
			bestDeviceIndex = i;
		}
		if (cpuDeviceIndex[i] && maxCalcUnits < numUnits[i]){
			maxCalcUnits = numUnits[i];
			bestDeviceIndex = i;
		}
	}
	if (bestDeviceIndex == -1){
		printf("error with geting the best device\n");
		exit(-1);
	}
	else {
		printf("\nworking device is: \n");
		clGetDeviceInfo(devices[bestDeviceIndex], CL_DEVICE_NAME, sizeof(char) * 100, devName, &size);
		printf("name :%s\n", devName);

		clGetDeviceInfo(devices[bestDeviceIndex], CL_DEVICE_VERSION, sizeof(char) * 100, devName, &size);
		printf("version :%s\n", devName);
		printf("compute units: %u\n\n", numUnits[bestDeviceIndex]);
	}
}

cl_device_id* Devices::getBestDevice() {
	return &devices[bestDeviceIndex];
}

void Context::createContext(cl_device_id* deviceForContext) {
	context = clCreateContext(NULL, 1, deviceForContext , NULL, NULL, &retError);
	checkError(retError, "context Error");
}

cl_context Context::getContext() {
	return context;
}

Programs::~Programs() {
	delete[] codeForKernel;
	clReleaseProgram(program);
}

void Programs::moveProgramToBuffer(const char* fileName) {
	FILE *clFile;
	fopen_s(&clFile, fileName, "r");

	if (!clFile) {
		printf("fail to open opencl programm file");
		exit(1);
	}
	fseek(clFile, 0, SEEK_END);
	maxCLProgSize = ftell(clFile);
	fseek(clFile, 0, SEEK_SET);
	codeForKernel = new char[maxCLProgSize];
	
	realCLProgSize = fread(codeForKernel, sizeof(char), maxCLProgSize, clFile);
	fclose(clFile);
}

void Programs::buildProgram(cl_context context, cl_device_id* device) {
	program = clCreateProgramWithSource(context, 1, (const char **)&codeForKernel, &realCLProgSize, &retError);
	retError |= clBuildProgram(program, 1, device, NULL, NULL, NULL);
	checkError(retError, "clBuildProgram");
	size_t realSize;

	clGetProgramBuildInfo(program, *device, CL_PROGRAM_BUILD_LOG, NULL, NULL, &realSize);
	char* errorBuffer = new char[realSize];

	clGetProgramBuildInfo(program, *device, CL_PROGRAM_BUILD_LOG, realSize, errorBuffer, NULL);

	std::cout << "error Buffer: " << errorBuffer[0] << std::endl;
}

cl_program Programs::getProgram() {
	return program;
}

CommandQueues::CommandQueues() {
	mass1 = new float[firstMassSize];
	mass2 = new float[secondMassSize];
	for (size_t i = 0; i < firstMassSize; ++i){
		mass1[i] = i;
	}
	for (size_t i = 0; i < secondMassSize; ++i){
		mass2[i] = i*1.5;
	}
	rezult = new float[rezultMassSize];
}

void CommandQueues::createQueue(cl_context context, cl_device_id device) {
	queue = clCreateCommandQueue(context, device,
		CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, NULL); // ���� ����� ������� �������� �� �������� ������
}

void CommandQueues::createBuffers(cl_context context) {
	mass1CL = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * firstMassSize, NULL, &retError);
	checkError(retError, "clCreateBuffer1");
	mass2CL = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * secondMassSize, NULL, &retError);
	checkError(retError, "clCreateBuffer2");
	rezultCL = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * rezultMassSize, NULL, &retError);
	checkError(retError, "clCreateBuffer");
	
	retError |= clEnqueueWriteBuffer(queue, mass1CL, CL_TRUE, 0, sizeof(float) * firstMassSize, mass1, 0, NULL, NULL);
	retError |= clEnqueueWriteBuffer(queue, mass2CL, CL_TRUE, 0, sizeof(float) * secondMassSize, mass2, 0, NULL, NULL);
	retError |= clEnqueueWriteBuffer(queue, rezultCL, CL_TRUE, 0, sizeof(float) * rezultMassSize, rezult, 0, NULL, NULL);
	checkError(retError, "clEnqueueWriteBuffer");
}

cl_command_queue CommandQueues::getQueue() {
	return queue;
}

void CommandQueues::releaseQueuesAndMem() {
	clFlush(queue);
	clFinish(queue);
	clReleaseMemObject(mass1CL);
	clReleaseMemObject(mass2CL);
	clReleaseMemObject(rezultCL);
	clReleaseCommandQueue(queue);
}

bool CommandQueues::checkMatrix() {
	unsigned matrixMultMistakes = 0;
	float* temp = new float[rezultMassSize];
	for (size_t i = 0; i < rezultMassSize; ++i){
		temp[i] = 0;
	}
	for (size_t i = 0; i < massH1; ++i){
		for (size_t j = 0; j < massW2; ++j){
			for (size_t k = 0; k < massW1H2; ++k){
				temp[i + j*massH1] += mass1[i * massW1H2 + k] * mass2[k * massW2 + j];
			}
		}
	}
	for (size_t i = 0; i < massH1; ++i){
		for (size_t j = 0; j < massW2; ++j){
			if (abs(temp[j * massH1 + i] - rezult[i * massW2 + j]) > 1e-5) {
				matrixMultMistakes++;
			}
		}
	}
	printf("matrix mistakes: %u \n", matrixMultMistakes);
	if (matrixMultMistakes == 0){
		return true;
	}
	else {
		return false;
	}
}

void CommandQueues::printMass(float* mass, int h, int w) {
	for (size_t i = 0; i < h; ++i){
		for (size_t j = 0; j < w; ++j){
			std::cout << mass[i * w + j] << " ";
		}
		std::cout << std::endl;
	}
}

Kernels::~Kernels() {
	clReleaseKernel(kernel);
}

void Kernels::createKernel(CommandQueues queues, cl_program program, const char* fincName) {
	kernel = clCreateKernel(program, fincName, &retError);
	retError |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &queues.mass1CL);
	retError |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &queues.mass2CL);
	retError |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &queues.rezultCL);
	retError |= clSetKernelArg(kernel, 3, sizeof(int), &queues.massW1H2);
	retError |= clSetKernelArg(kernel, 4, sizeof(int), &queues.massW2);
	checkError(retError, "clSetKernelArg");

	// ������� ����������� ��������� � ������ ��������� ��������� getLocalId
	// ��� ����� ������ ������ ������� ���������������� localItemSize - ������ ������
	// ����� ���� ��� �� ����������� ������, �� ������ ��������� ������, ����� ��� ������ ������ �����������
	// ����� ���� ��� ��� ����������� ���� ���� ��������� ������ 
	retError = clEnqueueNDRangeKernel(queues.getQueue(), kernel, 2, NULL,
		new size_t[]{ queues.massW2, queues.massH1 },
		new size_t[]{1, 1},
	0, NULL, &kernelEvent);
	checkError(retError, "clEnqueueNDRangeKernel");

	retError = clEnqueueReadBuffer(queues.getQueue(), queues.rezultCL, CL_TRUE, 0, sizeof(float) * queues.rezultMassSize, queues.rezult, 0, NULL, NULL);
	//clGetEventProfilingInfo(kernelEvent,  ); // ��� �������� ������� ������ �������
	checkError(retError, "clEnqueueReadBuffer");
	
	queues.releaseQueuesAndMem();
	//queues.printMass(queues.rezult, queues.massH1, queues.massW2);
	printf("\n\n");
	queues.checkMatrix();
}