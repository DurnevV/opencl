__kernel void mul(__global float* mass1, __global float* mass2, __global float* rezult, int w1h2, int w) {
	int tx = get_global_id(0); // 0 - ������ ������ �������
	int ty = get_global_id(1); // 1 - ������ ������ �������
	
	float value = 0;
	for (int k = 0; k < w1h2; ++k) {
		value += mass1[ty * w1h2 + k] * mass2[k * w + tx];
	}
	rezult[ty * w + tx] = value;
}