#include "CLInit.h"

cl_device_id *PlatformInfo::device = new cl_device_id[5];
cl_platform_id PlatformInfo::platformList;
cl_uint PlatformInfo::platformCount;
//cl_context_properties *PlatformInfo::prop = new cl_context_properties[3];
unsigned PlatformInfo::maxCLProgSize = 100;

OpenCLInitializer::OpenCLInitializer() {
	for (size_t i = 0; i < 10; ++i){
		for (size_t j = 0; j < 10; ++j){
//			mass1[i][j] = i + j;
//			mass2[i][j] = i - j;
		}
	}
}

OpenCLInitializer::~OpenCLInitializer(){
	clFlush(queue);
	clFinish(queue);
	// kernel / program
	clReleaseMemObject(mass1CL);
	clReleaseMemObject(mass2CL);
	clReleaseMemObject(rezultCL);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
	delete[] codeFromCLFile;
	//delete x;
	//delete y;
	//delete z;
}

void OpenCLInitializer::detectDeveces(){
	cl_uint retNumDevice = 0, 
		max_num_entries = 2; // ������������ ���������� �������� ������� �� ����� �����
	cl_device_type devType;
	char *devName = new char[100];
	size_t size, numUnits = 0;

	clGetPlatformIDs(max_num_entries, &PlatformInfo::platformList, &PlatformInfo::platformCount);
	printf("number of ocl platforms %u\n", PlatformInfo::platformCount);
	for (cl_uint i = 0; i < PlatformInfo::platformCount; ++i){
		clGetDeviceIDs(PlatformInfo::platformList, CL_DEVICE_TYPE_ALL, 2, PlatformInfo::device, &retNumDevice);
		printf("Number of the devices: %u\n", retNumDevice);
		for (cl_uint s = 0; s < retNumDevice; ++s){
			clGetDeviceInfo(PlatformInfo::device[s], CL_DEVICE_NAME, sizeof(char) * 100, devName, &size);
			printf("name :%s\n", devName);

			clGetDeviceInfo(PlatformInfo::device[s], CL_DEVICE_VERSION, sizeof(char) * 100, devName, &size);
			printf("version :%s\n", devName);

			clGetDeviceInfo(PlatformInfo::device[s], CL_DEVICE_TYPE, sizeof(cl_device_type), &devType, &size);
			printf("devType : %u\n", devType); // 4 - macro for GPU 2- macro for CPU

			clGetDeviceInfo(PlatformInfo::device[s], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(size_t), &numUnits, &size);
			printf("num units: %u\n next unit :\n", numUnits);
			printf("\n");
		}
	}
}

void OpenCLInitializer::createBuffAndCopyData() {
	//cl_context_properties props[3] = {
	//	CL_CONTEXT_PLATFORM, cl_context_properties(PlatformInfo::platformList), 0
	//}; // ��� ������ �� ����� �������

	size_t mass1Size = 100, mass2Size = 100, rezultSize = 100;
							//props	
	context = clCreateContext(NULL, 1, &PlatformInfo::device[1], NULL, NULL, &retError);
	checkError(retError, "context ");

	codeFromCLFile = new char[PlatformInfo::maxCLProgSize];

	realCLCodeSize = moveCLProgToBuff("clProgram.cl", codeFromCLFile);

	queue = clCreateCommandQueue(context, PlatformInfo::device[1],
		CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, NULL); // ���� ����� ������� �������� �� �������� ������
	mass1CL = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * mass1Size, NULL, &retError);
	mass2CL = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * mass2Size, NULL, &retError);
	rezultCL = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(int) * rezultSize, NULL, &retError);
	checkError(retError, "clCreateBuffer");

	retError = clEnqueueReadBuffer(queue, mass1CL, CL_TRUE, 0, sizeof(int) * mass1Size, mass1, 0, NULL, NULL);
	retError = clEnqueueReadBuffer(queue, mass2CL, CL_TRUE, 0, sizeof(int) * mass2Size, mass2, 0, NULL, NULL);
	retError = clEnqueueWriteBuffer(queue, rezultCL, CL_TRUE, 0, sizeof(int) * rezultSize, rezult, 0, NULL, NULL);
	checkError(retError, "clEnqueueWriteBuffer");
}

size_t OpenCLInitializer::moveCLProgToBuff(const char* fileName, char* codeFromCLFile) {
	FILE *clFile;
	fopen_s(&clFile, fileName, "r");
	
	if (!clFile){
		printf("fail to open opencl programm file");
		exit(1);
	}
	//fseek(clFile, 0, SEEK_END);
	//rewind(clFile);
	//codeFromCLFile = new char[PlatformInfo::maxCLProgSize+1];

	size_t realCLSize = fread(codeFromCLFile, sizeof(char), PlatformInfo::maxCLProgSize, clFile);
	
	fclose(clFile);
	return realCLSize;
}

cl_program OpenCLInitializer::buildProgram() {
	cl_program program = clCreateProgramWithSource(context, 1, (const char **)&codeFromCLFile, &realCLCodeSize, &retError);
	retError = clBuildProgram(program, 1, &PlatformInfo::device[1], NULL, NULL, NULL);
	checkError(retError, "clBuildProgram");
	size_t realSize;

	clGetProgramBuildInfo(program, PlatformInfo::device[1], CL_PROGRAM_BUILD_LOG, NULL, NULL, &realSize);
	char* errorBuffer = new char[realSize];

	clGetProgramBuildInfo(program, PlatformInfo::device[1], CL_PROGRAM_BUILD_LOG, realSize, errorBuffer, NULL);
	std::cout <<"error Buffer: " << errorBuffer;
	return program;
}

void OpenCLInitializer::createKernel(cl_program program) {
	cl_kernel kernel = clCreateKernel(program, "parallelAdd", &retError);
	retError = clSetKernelArg(kernel, 0, sizeof(cl_mem), &mass1CL);
	retError = clSetKernelArg(kernel, 1, sizeof(cl_mem), &mass2CL);
	retError = clSetKernelArg(kernel, 2, sizeof(cl_mem), &rezultCL);
	size_t globalItemSize = 1, localItemSize = 1; 
	// ������� ����������� ��������� � ������ ��������� ��������� getLocalId
	// ��� ����� ������ ������ ������� ���������������� localItemSize - ������ ������
	checkError(retError, "clSetKernelArg"); 
	// ����� ���� ��� �� ����������� ������, �� ������ ��������� ������, ����� ��� ������ ������ �����������
	// ����� ���� ��� ��� �����������  ���� ���� ��������� ������
	retError = clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
		&globalItemSize, &localItemSize, 0, NULL, NULL);
	checkError(retError, "clEnqueueNDRangeKernel");
	
	retError = clEnqueueReadBuffer(queue, rezultCL, CL_TRUE, 0, sizeof(int), rezult, 0, NULL, NULL);
	checkError(retError, "clEnqueueReadBuffer");
	
	
	clReleaseKernel(kernel);
	clReleaseProgram(program);
}

void OpenCLInitializer::checkError(cl_int err, const char* name){
	if (err != CL_SUCCESS){
		std::cout << err << " " <<  name << std::endl;
	}
}

void OpenCLInitializer::initializeMass(size_t rows, size_t cols){
	mass1 = new int[rows*cols];
	mass2 = new int[rows*cols];
	
	for (size_t i = 0; i < rows; ++i){
		for (size_t j = 0; j < cols; ++j){

		}
	}
}