#include "Host.hpp"

Host::Host() {
	platforms.getPlatformIDs();
	devices.getAllPlatformDevices(platforms);
	devices.initBestDevice();
	context.createContext(devices.getBestDevice());
}

void Host::runProgram(const char* fileName, const char* funcName) {
	programs.moveProgramToBuffer(fileName);
	programs.buildProgram(context.getContext(), devices.getBestDevice());
	
	commandQueues.createQueue(context.getContext(), *devices.getBestDevice());
	commandQueues.createBuffers(context.getContext());
	
	kernels.createKernel(commandQueues, programs.getProgram(), funcName);
}