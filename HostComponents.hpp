#ifndef _HC
#define _HC

#include <CL\opencl.h>
#include "ErrorHandler.hpp"
#include <iostream>

class Platforms {
public:	
	void getPlatformIDs();
	cl_uint getPlatformNumber();
	cl_platform_id getPlatformList();
	
private:
	struct PlatformInfo {
		cl_platform_id platformList;
		cl_uint platformCount;
	} platforms;
};

class Devices {
public:
	Devices();
	void getAllPlatformDevices(const Platforms);
	void initBestDevice();
	cl_device_id* getBestDevice();

private:
	cl_device_id* devices;
	short numUnits[5], bestDeviceIndex = -1;
	cl_uint retNumDevice = 0;
	bool gpuDeviceIndex[5], 
		cpuDeviceIndex[5];
};

class Context : public ErrorHandler {
public:
	~Context(){
		clReleaseContext(context);
	}
	void createContext(cl_device_id*);
	cl_context getContext();

private:
	cl_context context;
};

class Programs : public ErrorHandler {
public:
	Programs(){}
	~Programs();
	void moveProgramToBuffer(const char*);
	void buildProgram(cl_context, cl_device_id*);
	cl_program getProgram();

private:
	unsigned maxCLProgSize;
	size_t realCLProgSize;
	char* codeForKernel;
	cl_program program;
};

class CommandQueues : public ErrorHandler {
public:
	CommandQueues();
	
	void releaseQueuesAndMem();
	void createQueue(cl_context, cl_device_id);
	void createBuffers(cl_context);
	cl_command_queue getQueue();
	void printMass(float*, int, int);
	bool checkMatrix();

//��������� �������, ����� ��������� � ������ �����(��������� ������� �����������)
	size_t massW1H2 = 5, massH1 = 8, massW2 = 5;
	size_t firstMassSize = massW1H2*massH1;
	size_t secondMassSize = massW1H2*massW2;
	size_t rezultMassSize = massH1*massW2;
	cl_mem mass1CL, mass2CL, rezultCL;
	float *mass1, *mass2, *rezult;

private :
	cl_command_queue queue;
};

class Kernels : public ErrorHandler {
public:
	~Kernels();

	void createKernel(CommandQueues, cl_program, const char*);

private:
	cl_event kernelEvent;
	cl_kernel kernel;
};

#endif _HC