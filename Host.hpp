#ifndef _HOST
#define _HOST

#include "HostComponents.hpp"

class Host {
public:
	Host();
	void runProgram(const char*, const char*);

private:
	Platforms platforms;
	Devices devices;
	Context context;
	Programs programs;
	CommandQueues commandQueues;
	Kernels kernels;
};

#endif //_HOST