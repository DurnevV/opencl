#ifndef _EH
#define _EH

#include <iostream>

// надо продумать автоматического слушателя error переменной
class ErrorHandler {
protected:
	void checkError(cl_int err, const char* name) {
		if (err != CL_SUCCESS){
			std::cout << err << " " << name << std::endl;
		}
	};
	cl_int retError = 0;
};

#endif //_EH